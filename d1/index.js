// alert("Hello, Batch 241");

//arithmetic operators
let x = 1397;
let y = 7831;

let sum = x+y;
console.log("Result of addition operator: " + sum);


let diff = x-y;
console.log("Result of subtraction operator: " + diff);

let prod = x*y;
console.log("Result of multipilcation operator: "+prod);

let div = x/y;
console.log("Result of division operator: " + div);

let remainder = y%x;
console.log("Result of Modulo operator: " + remainder);


//Assignment operator
// Basic assignment operator (=)
/*
	The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
*/

let assignmentNumber = 8;

//Addition Assignment operator(+=)
/*
	Assigns the variable with the sum of its current value and the value after the operator.
*/
assignmentNumber += 2;
console.log("Result of the addition assignment operator: " + assignmentNumber);

//Subtraction/Multiplication/Division Assignment operator:
assignmentNumber -= 2;
console.log("Result of the subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of the multipilcation assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of the division assignment operator: " + assignmentNumber);

//Multiple Operators and Parentheses
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS Rule (Parenthesis, Exponents, Multiplication, Division, Addition, and Subtraction)

*/

let mdas = 1+2-3*4/5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1+(2-3) * (4/5);
console.log("Result of pemdas operation: " + pemdas);

let sample = 5+ 2 % 10;
console.log(sample);

// Increment and Decrement

let z = 1;

//Pre-Increment
// let preIncrement = ++z;

// console.log("Result of the pre-increment: " + preIncrement);

// console.log("Result of the pre-increment: " + z);



//In post-increment, the value of z is first assigned to postIncrement before it is increased by one;
let postIncrement = z++;
console.log("Result of the post-increment: " + postIncrement);
console.log("Result of the post-increment: " + z);


let a = 2;
//Pre-decrement:
/*let preDecrement = --a;
console.log("Result of the pre-decrement: " + preDecrement);
console.log("Result of the pre-decrement: " + a);*/

//Post-decrement:
let postDecrement = a--;
console.log("Result of the post-decrement: " + postDecrement);
console.log("Result of the post-decrement: " + a);

// Type Coercion
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another
*/

let numA = "10"; //string
let numB = 12; //number

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion) //string


let coercion1 = numA - numB;
console.log(coercion1); //1012
console.log(typeof coercion1) //string


//Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion)


//Addition of Number and Boolean
let numE = true + 1;
console.log(numE);
console.log(typeof numE);

let numF = false + 1;
console.log(numF);
console.log(typeof numF);


// Comparison operator
let juan = 'juan';

//Equality Operator (==)
console.log(1==1);
console.log(1==2);
console.log(1== '1');
console.log(1 == true);

console.log('juan' == 'juan');
console.log(true == '1');
console.log(juan == 'juan');

//Inequality operator
/*
	Checks whether the operands are not equal
*/
console.log("Inequality: ")
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(1 != true);
console.log('juan' != 'juan');
console.log('juan' != juan);

//Strict Equaltiy Operator(===)
/*
	Checks whether the operands have the same value AND data type
*/
console.log("Strict equality operator")
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(1 === true);
console.log('juan' === 'juan');
console.log(juan === 'juan');

//Strict Inequality Operator (!==)
/*
	Checks whether the operands do not have the same value AND data type
*/

console.log("Strict inequality operator")
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(1 !== true);
console.log('juan' !== 'juan');
console.log(juan !== 'juan');

//Relational operator
let j = 50;
let k = 65;

//GT
	console.log("Greater than operator")
	let gT = j>k;
	console.log(gT);

//LT 
	console.log("Less than or equal operator")
	let	lT = j<k;
	console.log(lT);

//GTE
	console.log("Greater than or equal operator")
	let gTE = j>=k;
	console.log(gTE);

//LT 
	console.log("Less than or equal operator")
	let	lTE = j<=k;
	console.log(lTE);

let numStr = '30';
//forced coercion to change the string to a number
console.log(j>numStr);

// Logical operators

